# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 11:28:17 2020

@author: biagi
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 18:28:19 2020

@author: biagi
"""
# pivacliente, tipo, dataprenotazione, pivafornitore, quantita
import string
import random

def makedate():
	anno = random.randint(1991, 2020)
	mese = random.randint(1,12)
	giorno = random.randint(1, 28)
	data = str(anno) + '-' + str(mese) + '-' + str(giorno)
	return data


file = open("deposito.txt", 'w') #idnotifica, tipo, quantitaScaricata, quantitaRimasta, prezzo
file2 = open("deposito2.txt", 'w')
Fnotifica = open("notificascarico2.txt", 'r') #data_scarico, data_notifica, id_notifica, pivafornitore, 
Ffornisce = open("fornisce.txt",'r') #pivafornitore, tipo
j = 0

notifiche = []
forniture = []
used = []

def validate(validset, piva):
    for x in validset:
        if (piva, x) not in(forniture): print("errore")

for index in range(0, 6000):
	nots = Fnotifica.readline().split('\t')
	notifiche.append((nots[2], nots[3][:-1], nots[0]))

#print(notifiche)

for index in range(0, 450):
	forns = Ffornisce.readline().split('\t')
	forniture.append((forns[0], forns[1][:-1]))
print(len(forniture))
#print(forniture)
#notifiche : id_notifica, pivafornitore, data_scarico
#forniture : pivafornitore, tipo
file.write("insert into depositaprodotto(idnotifica, tipo, quantitaScaricata, quantitaRimasta, prezzo) values \n")
for notifica in notifiche:
	forns = None
	validset = []
	for x in forniture:
		if x[0] == notifica[1]: validset.append(x[1])
	#print(validset)
	validate(validset, notifica[1])
	forns = random.choice(validset)
	validset = []
	tipo = forns
	idnotifica = notifica[0]
	quantitaRimasta = random.randint(10, 5000)
	quantitaScaricata = random.randint(quantitaRimasta, 17000)
	prezzo = round(random.uniform(1.0, 5.0), 2)
	file.write('('+idnotifica+',\''+tipo+'\','+str(quantitaScaricata)+','+str(quantitaRimasta)+','+str(prezzo)+'),\n')

Ffornisce.close()
Fnotifica.close()
file2.close()
file.close()