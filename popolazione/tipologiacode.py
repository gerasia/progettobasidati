# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:31:39 2020

@author: biagi
"""


import string
import random

file = open("tipologia", 'w')

j = 0
tipi = []


while j < 300:
	tipo = ""
	for i in range(0, random.choice(range(3,10))):
		tipo = tipo + random.choice(string.ascii_letters)
	if tipo not in tipi:
		tipi.append(tipo)
		j = j + 1
		file.write(tipo+'\n')

file.close()