# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 18:28:19 2020

@author: biagi
"""
# pivacliente, tipo, dataprenotazione, pivafornitore, quantita
import random

def makedate():
	anno = random.randint(1991, 2020)
	mese = random.randint(1,12)
	giorno = random.randint(1, 28)
	data = str(anno) + '-' + str(mese) + '-' + str(giorno)
	return data

file = open("prenotazione.txt", 'w')
cliente = open("cliente.txt", 'r')
Ffornisce = open("fornisce.txt", 'r')
j = 0

pivcli = []
fornitoritipi = []
for j in range(0,300):
	y = Ffornisce.readline().split('\t')
	fornitoritipi.append((y[0], y[1][:-1]))

print(fornitoritipi)
for j in range(0, 100):
	y = cliente.readline().split('\t')
	pivcli.append(y[0])

j = 0
file.write("insert into prenotazione(pivacliente, tipo, dataprenotazione, pivafornitore, quantita) values\n")
while j < 1000:
	quantita = random.randint(1000,100000)
	fornitore, tipo = random.choice(fornitoritipi)
	pivc = random.choice(pivcli)
	data = makedate()
	j = j + 1
	file.write('(\''+pivc +'\',\''+ tipo +'\',\''+ data + '\',\'' + fornitore + '\',' + str(quantita) + '),\n')
    
cliente.close()
Ffornisce.close()
file.close()