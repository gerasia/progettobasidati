# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 17:46:19 2020

@author: biagi
"""

import string
import random

def checkdate(anno1, mese1, giorno1, anno2, mese2, giorno2):
    if anno2 > anno1 : return True
    if anno2 < anno1 : return False
    if mese2 > mese1 : return True
    if mese2 < mese1 : return False
    if giorno2 > giorno1 : return True
    return False

file = open("notificascarico.txt", 'w')
file2 = open("fornitore.txt", 'r')
file3 = open("notificascarico2.txt",'w')
j = 0
pive = []

for j in range(0,150):
	y = file2.readline().split('\t')
	pive.append(y[0])

j = 0
file.write("insert into notificascarico(data_scarico,data_notifica,id_notifica,pivafornitore) values\n")
while j < 6000:
	annonotifica = random.randint(1991, 2020)
	mesenotifica = random.randint(1,12)
	giornonotifica = random.randint(1, 28)
	annoscarico = random.randint(1991, 2020)
	mesescarico = random.randint(1,12)
	giornoscarico = random.randint(1,28)
	valid = checkdate(annonotifica, mesenotifica, giornonotifica, annoscarico, mesescarico, giornoscarico)
	if valid == True :        
		datascarico = str(annoscarico) + '-' + str(mesescarico) + '-' + str(giornoscarico)
		datanotifica = str(annonotifica) + '-' + str(mesenotifica) + '-' + str(giornonotifica)
		piva = random.choice(pive)
		j = j + 1
		file3.write(datascarico+'\t'+datanotifica+'\t'+str(j)+'\t'+piva+'\n')
		file.write("(\'"+datascarico + '\',\'' + datanotifica + '\',' + str(j) + ',\'' + piva + '\'),\n')
file.write(";")
file3.close()
file2.close()
file.close()