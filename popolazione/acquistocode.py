# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 18:28:19 2020

@author: biagi
"""
# pivacliente, tipo, dataprenotazione, pivafornitore, quantita
import random

def makedate():
	anno = random.randint(1991, 2021)
	mese = random.randint(1,12)
	giorno = random.randint(1, 28)
	data = str(anno) + '-' + str(mese) + '-' + str(giorno)
	return data

file = open("acquisto.txt", 'w')
cliente = open("cliente.txt", 'r')
Fnotifica = open("notificascarico.txt", 'r')
Fdeposito = open("deposito.txt", 'r')
j = 0

pivcli = []
notifiche = []
depositi = []
for j in range(0,6000):
	y = Fnotifica.readline().split('\t')
	z = Fdeposito.readline().split('\t')
	depositi.append((z[0], z[3]))
	notifiche.append(y[2])

for j in range(0, 100):
	y = cliente.readline().split('\t')
	pivcli.append(y[0])

j = 0
file.write("insert into acquisto(pivacliente, quantita, prezzo, dataacquisto, id, iddeposito) values\n")
while j < 1000:
	quantita = random.randint(1000,100000)
	notifica = random.choice(notifiche)
	prezzo = round(random.uniform(1.0 , 10.0), 2)
	pivc = random.choice(pivcli)
	data = makedate()
	j = j + 1
	file.write('(\''+pivc +'\','+ str(quantita) +','+ str(prezzo) + ',\'' + data + '\',' + str(j) + ',\''+notifica+'\'),\n')
    
cliente.close()
Fnotifica.close()
file.close()