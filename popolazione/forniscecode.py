# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:45:50 2020

@author: biagi
"""

import random

file = open("fornisce.txt", 'w')
file2 = open("tipologia.txt", 'r')
file3 = open("fornitore.txt", 'r')
j = 0
tipi = []
fornitori = []
couple = []
for j in range(0,300):
	y = file2.readline().split('\t')
	for x in y:
		tipi.append(x)

for j in range(0, 150):
	y = file3.readline().split('\t')
	fornitori.append(y[0])

j = 0
file.write('insert into fornitoretipo(pivafornitore, tipo) values\n')
for forn in fornitori:
	tipo = random.choice(tipi)
	tipo = tipo[:-1]
	if (forn, tipo) not in couple:
		couple.append((forn,tipo))
		j = j + 1
		file.write('(\'' + forn + '\', \'' + tipo + '\'),\n')

while j < 450:
	tipo = random.choice(tipi)
	tipo = tipo[:-1]
	fornitore = random.choice(fornitori)
	if (fornitore, tipo) not in couple:
		couple.append((fornitore,tipo))
		j = j + 1
		file.write('(\'' + fornitore + '\', \'' + tipo + '\'),\n')
file.write(';')
file2.close()
file.close()