delimiter //
create trigger preavviso
after insert on prenotazione
for each row
begin
	declare x int;
	select new.quantita/1000 into x;
	if new.dataprenotazione - current_date() < x then
		delete from prenotazione where pivacliente = new.pivacliente and
			dataprenotazione = new.dataprenotazione and tipo = new.tipo and
			pivafornitore = new.pivafornitore;
	end if;
end;
//
delimiter ;

delimiter //
create trigger notificavalida
after insert on notificascarico
for each row
begin
	if new.data_scarico - new.data_notifica < 1 then
		delete from notificascarico where id_notifica = new.id_notifica;
	end if;
end;
//
delimiter ;


delimiter //
create trigger quantitadisponibile
before insert on acquisto
for each row
begin
	declare x bigint;
	declare y bigint;
	(select quantitarimasta into y from depositaprodotto
	where new.iddeposito = idnotifica);
	if y = 0 then
		signal sqlstate '45000' set message_text = 'PRODUCT NOT AVAILABLE!';
	end if;
	if new.quantita > y then
		set new.quantita = y;
		update depositaprodotto set quantitarimasta = 0
		where new.iddeposito = idnotifica;
	else
		(select quantitarimasta - new.quantita into x from depositaprodotto
		where new.iddeposito = idnotifica);
		update depositaprodotto set quantitarimasta = x
		where new.iddeposito = idnotifica;
	end if;
end
//
delimiter ;

delimiter //
create trigger checkingrosso
before insert on acquisto
for each row
begin
	if new.quantita <= 5 and new.pivacliente is not null then
		set new.pivacliente = null;
	elseif new.quantita > 5 and new.pivacliente is null then
		signal sqlstate '45000' set message_text = 'QUANTITY > 5 AND CLIENTE IS NULL';
	end if;
end;
//
delimiter ;

delimiter //
create trigger checkguadagno
before insert on acquisto
for each row
begin
	declare x double;
	select prezzo into x from depositaprodotto where idnotifica = new.iddeposito;
	if new.prezzo < x - (x*0.01) then
		signal sqlstate '45001' set message_text = 'PRICE LOWER THAN LIMIT';
	end if;
end;
//
delimiter ;

delimiter //
create trigger consistenzaprodotto
before insert on depositaprodotto
for each row
begin
	declare pivaforn varchar(11);
	(select pivafornitore into pivaforn from notificascarico ns where new.idnotifica = ns.id_notifica);
	if not exists(select * from fornitoretipo where tipo = new.tipo and pivafornitore = pivaforn) then
		signal sqlstate '45002' set message_text = 'THE SUPPLIER DOES NOT PRODUCE THIS TYPE OF PRODUCT';
	end if;
end;//

delimiter ;

delimiter //
create trigger consistenzaprenotazione
after insert on prenotazione
for each row
begin
	if not exists (select * from fornitoretipo 
		where new.pivafornitore = pivafornitore and new.tipo = tipo) then
		delete from prenotazione
		where pivacliente = new.pivacliente and
			  pivafornitore = new.pivafornitore and
			  tipo = new.tipo and
			  dataprenotazione = new.dataprenotazione;
	end if;
end; //
delimiter ;

delimiter //
create trigger checkdataacquisto
before insert on acquisto
for each row
begin 
	if new.dataacquisto < (select data_scarico from notificascarico
		where new.iddeposito = id_notifica) then
		signal sqlstate '45003' set message_text = 'ACQUISTO\'S DATE IS LOWER THAN THE DEPOSITO\'S DATE';
	end if;
end;//
delimiter ;

create event depositomancato
on schedule every 1 day
starts (timestamp(current_date()) + interval 1 day + interval 1 hour)
do 
	delete from notificascarico ns 
	where data_scarico < current_date() - 1 and 
	not exists (select * from deposito d 
			    where d.idnotifica = ns.id_notifica);
























-------------------------------------------------------------------------------
delimiter //
create trigger temporaneo
before insert on acquisto
for each row
begin
	if new.dataacquisto < (select data_scarico from notificascarico where new.iddeposito = id_notifica) then
		set new.dataacquisto = (select data_scarico from notificascarico where new.iddeposito = id_notifica) + 1;
	end if;
end;//
delimiter ;

delimiter //
create trigger temp2
	before insert on acquisto
	for each row
	begin 
		declare x double;
		select prezzo into x from depositaprodotto where idnotifica = new.iddeposito;
		if new.prezzo < x - (x*0.01) then
			set new.prezzo = x - (x*0.01);
		end if;
end;//
delimiter ;

delimiter //
create trigger temp3
	after insert on acquisto
	for each row
	begin
		declare x int;
		select quantitaScaricata into x from depositaprodotto where idnotifica = new.iddeposito;
		update depositaprodotto set quantitaScaricata = x + new.quantita;
	end;
//
delimiter ;